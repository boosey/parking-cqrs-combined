package org.boosey.parking.space.rest.api;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.path.json.*;
import io.restassured.response.Response;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import static org.hamcrest.MatcherAssert.*;
import java.util.ArrayList;
import javax.ws.rs.core.Response.Status;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.hamcrest.Matcher;
import static org.hamcrest.Matchers.*;
import org.hamcrest.beans.*;
import static io.restassured.RestAssured.*;

@QuarkusTest
@TestInstance(Lifecycle.PER_CLASS)
public class SpaceResourceTest {
    private static final String spacePath = "/space";
    private static  SpaceMock newSpace1, newSpace2, newSpace3;
    protected static Gson gson = new Gson();
    protected static TypeToken<ArrayList<SpaceMock>>  SpaceMockCollectionType = new TypeToken<ArrayList<SpaceMock>>(){};

    @SuppressWarnings("unused")
    private static class SpaceMock {
        public String id;
        public String number;
        public String ownerId;
        public String residentId;
        public Boolean residentApprovedByOwner;
    }

    JsonPath jsonPath;

    @BeforeAll
    static void initTestObjects() {

        newSpace1 = new  SpaceMock();
        newSpace1.number = "204";

        newSpace2 = new  SpaceMock();
        newSpace2.number = "8293";

        newSpace3 = new  SpaceMock();
        newSpace3.number = "10294";
    }

    @Test
    public void simpleStressTest() {
        for (int i = 0; i < 10; i++) {
            listAll();
        }
    }

    @Test
    public void deleteAll() {
        ArrayList<SpaceMock> s_list;

        given()
            .when().delete(spacePath)
            .then()
                .statusCode(Status.OK.getStatusCode());

        s_list = given()
                .when().get(spacePath)
                .then()
                    .statusCode(Status.OK.getStatusCode())
                    .contentType(ContentType.JSON)
                    .extract().as( SpaceMockCollectionType.getType());
        
        assertThat(s_list, hasSize(0));   
        assertThat(s_list, is(empty()));    
    }

    @Test
    public void addSomeSpaces() {

        deleteAll();

         SpaceMock s = given().contentType(ContentType.JSON).body(newSpace1).when().post(spacePath).then()
                .statusCode(Status.CREATED.getStatusCode())
                .contentType(ContentType.JSON)
                .extract().as( SpaceMock.class, ObjectMapperType.GSON);

        assertThat(s,  beanMatcher(newSpace1, "id"));
        
        s = given().contentType(ContentType.JSON).body(newSpace2).when().post(spacePath).then()
                .statusCode(Status.CREATED.getStatusCode())
                .contentType(ContentType.JSON)
                .extract().as( SpaceMock.class, ObjectMapperType.GSON);

        assertThat(s,  beanMatcher(newSpace2, "id"));
    }

    @Test
    public void unknownIdRetrieval() {
        getSpaceById("5e377ad874760b05ddb625bf")
            .then()
                .statusCode(Status.NOT_FOUND.getStatusCode());
    }

    public Response getSpaceById(String id) {
        return given()
         .contentType(ContentType.JSON)
        .when()
            .get(spacePath + "/{id}", id);            
    }
    
    @Test
    public void listAll() {

        ArrayList<SpaceMock> s_list;
        addSomeSpaces();

        s_list = given()
          .when().get(spacePath)
          .then()
             .statusCode(Status.OK.getStatusCode())
             .contentType(ContentType.JSON)
             .extract().as( SpaceMockCollectionType.getType());
        
        assertThat(s_list, hasSize(2));
        assertThat(s_list.get(0),  beanMatcher(newSpace1, "id"));
        assertThat(s_list.get(1),  beanMatcher(newSpace2, "id"));
    }

    protected <T> Matcher<T> beanMatcher(T s, String ...ignore) {
        return SamePropertyValuesAs.<T>samePropertyValuesAs(s, ignore);
    }
}

