package org.boosey.parking.events;

import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;

@ToString
@ApplicationScoped
public class CompleteOwnerUpdateOwnerIsResident extends EventBase<OwnerDeletedCompleteEvent> {

    @Inject @Channel("complete-owner-update-owner-is-resident") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "owner_events")
    public class CompleteOwnerUpdateOwnerIsResidentData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public String ownerId;
        public Boolean ownerIsResident;
    }

    @PostConstruct
    public void init() {
        this.configure("complete-owner-update-owner-is-resident", concreteEmitter, new CompleteOwnerUpdateOwnerIsResidentData());
    }

    public CompleteOwnerUpdateOwnerIsResident with(UUID commandId, String ownerId, Boolean ownerIsResident) {
        CompleteOwnerUpdateOwnerIsResidentData evtData = (CompleteOwnerUpdateOwnerIsResidentData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.prepareCommandId(commandId);
        evtData.ownerId = ownerId;
        evtData.ownerIsResident = ownerIsResident;
        return this;
    }
}
