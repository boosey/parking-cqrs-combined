package org.boosey.parking.events;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;

@ToString
@ApplicationScoped
public class DeleteAllOwnersCommandEvent extends EventBase<DeleteAllOwnersCommandEvent> {

    @Inject @Channel("delete-all-owners-command") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "owner_events")
    public class DeleteAllOwnersCommandEventData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public Long count;
    }

    @PostConstruct
    public void init() {
        this.configure("delete-all-owners-command", concreteEmitter, new DeleteAllOwnersCommandEventData());
    }

    public DeleteAllOwnersCommandEvent with(Long count) {
        DeleteAllOwnersCommandEventData evtData = (DeleteAllOwnersCommandEventData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.getNewCommandId().toString();
        evtData.count = count;
        return this;
    }
}