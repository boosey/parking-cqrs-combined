package org.boosey.parking.events;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;

@ToString
@ApplicationScoped
public class CommandOwnerUpdateDetails extends EventBase<CommandOwnerUpdateDetails> {

    @Inject @Channel("command-owner-update-details") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "owner_events")
    public class CommandOwnerUpdateDetailsData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public String ownerId;
        public String name;
        public String email;
        public String phone;
    }

    @PostConstruct
    public void init() {
        this.configure("command-owner-update-details", concreteEmitter, new CommandOwnerUpdateDetailsData());
    }

    public CommandOwnerUpdateDetails with(String ownerId, String name, String email, String phone) {
        CommandOwnerUpdateDetailsData evtData = (CommandOwnerUpdateDetailsData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.getNewCommandId().toString();
        evtData.ownerId = ownerId;
        evtData.name = name;
        evtData.email = email;
        evtData.phone = phone;
        return this;
    }
}