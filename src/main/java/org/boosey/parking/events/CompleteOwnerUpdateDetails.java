package org.boosey.parking.events;

import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;


@ToString
@ApplicationScoped
public class CompleteOwnerUpdateDetails extends EventBase<OwnerDeletedCompleteEvent> {

    @Inject @Channel("complete-owner-update-details") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "owner_events")
    public class CompleteOwnerUpdateDetailsData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public String ownerId;
        public String name;
        public String email;
        public String phone;
    }

    @PostConstruct
    public void init() {
        this.configure("complete-owner-update-details", concreteEmitter, new CompleteOwnerUpdateDetailsData());
    }

    public CompleteOwnerUpdateDetails with(UUID commandId, String ownerId, String name, String email, String phone) {
        CompleteOwnerUpdateDetailsData evtData = (CompleteOwnerUpdateDetailsData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.prepareCommandId(commandId);
        evtData.ownerId = ownerId;
        evtData.name = name;
        evtData.email = email;
        evtData.phone = phone;
        return this;
    }
}
