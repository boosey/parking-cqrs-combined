package org.boosey.parking.events;

import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;

@ToString
@ApplicationScoped
public class DeleteSpaceCommandEvent extends EventBase<DeleteSpaceCommandEvent> {

    @Inject @Channel("delete-space-command") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "space_events")
    public class DeleteSpaceCommandEventData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public String spaceId;
        public Boolean isIndividualDeletion;
    }

    @PostConstruct
    public void init() {
        this.configure("delete-space-command", concreteEmitter, new DeleteSpaceCommandEventData());
    }

    public DeleteSpaceCommandEvent with(UUID spaceId, Boolean isIndividualDeletion) {
        DeleteSpaceCommandEventData evtData = (DeleteSpaceCommandEventData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.getNewCommandId().toString();
        evtData.spaceId = spaceId.toString();
        evtData.isIndividualDeletion = isIndividualDeletion;
        return this;
    }
}