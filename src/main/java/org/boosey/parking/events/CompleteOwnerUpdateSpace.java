package org.boosey.parking.events;

import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;

@ToString
@ApplicationScoped
public class CompleteOwnerUpdateSpace extends EventBase<OwnerDeletedCompleteEvent> {

    @Inject @Channel("complete-owner-update-space") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "owner_events")
    public class CompleteOwnerUpdateSpaceData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public String ownerId;
        public String spaceId;
    }

    @PostConstruct
    public void init() {
        this.configure("complete-owner-update-space", concreteEmitter, new CompleteOwnerUpdateSpaceData());
    }

    public CompleteOwnerUpdateSpace with(UUID commandId, String ownerId, String spaceId) {
        CompleteOwnerUpdateSpaceData evtData = (CompleteOwnerUpdateSpaceData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.prepareCommandId(commandId);
        evtData.ownerId = ownerId;
        evtData.spaceId = spaceId;
        return this;
    }
}
