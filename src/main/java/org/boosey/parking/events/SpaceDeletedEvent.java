package org.boosey.parking.events;

import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;

@ToString
@ApplicationScoped
public class SpaceDeletedEvent extends EventBase<SpaceDeletedEvent> {

    @Inject @Channel("space-deleted-complete") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "space_events")
    public class SpaceDeletedEventData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public String spaceId;
        public Boolean isIndividualDeletion;
    }

    @PostConstruct
    public void init() {
        this.configure("space-deleted-complete", concreteEmitter, new SpaceDeletedEventData());
    }

    public SpaceDeletedEvent with(UUID commandId, UUID spaceId, Boolean isIndividualDeletion) {
        SpaceDeletedEventData evtData = (SpaceDeletedEventData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.prepareCommandId(commandId);
        evtData.spaceId = spaceId.toString();
        evtData.isIndividualDeletion = isIndividualDeletion;
        return this;
    }
}