package org.boosey.parking.events;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;

@ToString
@ApplicationScoped
public class CommandOwnerUpdateSpace extends EventBase<CommandOwnerUpdateSpace> {

    @Inject @Channel("command-owner-update-space") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "owner_events")
    public class CommandOwnerUpdateSpaceData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public String ownerId;
        public String spaceId;
    }

    @PostConstruct
    public void init() {
        this.configure("command-owner-update-space", concreteEmitter, new CommandOwnerUpdateSpaceData());
    }

    public CommandOwnerUpdateSpace with(String ownerId, String spaceId) {
        CommandOwnerUpdateSpaceData evtData = (CommandOwnerUpdateSpaceData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.getNewCommandId().toString();
        evtData.ownerId = ownerId;
        evtData.spaceId = spaceId;
        return this;
    }
}