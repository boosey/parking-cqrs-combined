package org.boosey.parking.events;

import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;


@ToString
@ApplicationScoped
public class OwnerDeletedCompleteEvent extends EventBase<OwnerDeletedCompleteEvent> {
    
    @Inject @Channel("owner-deleted-complete") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "owner_events")
    public class OwnerDeletedCompleteEventData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public String ownerId;
        public Boolean isIndividualDeletion;
    }

    @PostConstruct
    public void init() {
        this.configure("owner-deleted-complete", concreteEmitter, new OwnerDeletedCompleteEventData());
    }

    public OwnerDeletedCompleteEvent with(UUID commandId, UUID ownerId, Boolean isIndividualDeletion) {
        OwnerDeletedCompleteEventData evtData = (OwnerDeletedCompleteEventData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.prepareCommandId(commandId);
        evtData.ownerId = ownerId.toString();
        evtData.isIndividualDeletion = isIndividualDeletion;
        return this;
    }
}
