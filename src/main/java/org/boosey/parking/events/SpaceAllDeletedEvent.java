package org.boosey.parking.events;

import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;

@ToString
@ApplicationScoped
public class SpaceAllDeletedEvent extends EventBase<SpaceAllDeletedEvent> {

    @Inject @Channel("space-all-deleted-complete") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "space_events")
    public class SpaceAllDeletedEventData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public Long count;
    }

    @PostConstruct
    public void init() {
        this.configure("space-all-deleted-complete", concreteEmitter, new SpaceAllDeletedEventData());
    }

    public SpaceAllDeletedEvent with(UUID commandId, Long count) {
        SpaceAllDeletedEventData evtData = (SpaceAllDeletedEventData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.prepareCommandId(commandId);
        evtData.count = count;
        return this;
    }
}