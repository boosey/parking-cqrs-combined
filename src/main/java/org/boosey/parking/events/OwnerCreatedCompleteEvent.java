package org.boosey.parking.events;

import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;


@ToString
@ApplicationScoped
public class OwnerCreatedCompleteEvent extends EventBase<OwnerDeletedCompleteEvent> {

    @Inject @Channel("owner-created-complete") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "owner_events")
    public class OwnerCreatedCompleteEventData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public String ownerId;
        public String number;
        public String spaceId;
        public String residentId;
        public Boolean residentApprovedByOwner;
    }

    @PostConstruct
    public void init() {
        this.configure("owner-created-complete", concreteEmitter, new OwnerCreatedCompleteEventData());
    }

    public OwnerCreatedCompleteEvent with(UUID commandId, String ownerId) {
        OwnerCreatedCompleteEventData evtData = (OwnerCreatedCompleteEventData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.prepareCommandId(commandId);
        evtData.ownerId = ownerId;
        return this;
    }
}
