package org.boosey.parking.events;

import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;

@ToString
@ApplicationScoped
public class OwnerAllDeletedCompleteEvent extends EventBase<OwnerAllDeletedCompleteEvent> {
    @Inject @Channel("owner-all-deleted-complete") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "owner_events")
    public class OwnerAllDeletedCompleteEventData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public Long count;
    }

    @PostConstruct
    public void init() {
        this.configure("owner-all-deleted-complete", concreteEmitter, new OwnerAllDeletedCompleteEventData());
    }

    public OwnerAllDeletedCompleteEvent with(UUID commandId, Long count) {
        OwnerAllDeletedCompleteEventData evtData = (OwnerAllDeletedCompleteEventData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.prepareCommandId(commandId);
        evtData.count = count;
        return this;
    }
}
