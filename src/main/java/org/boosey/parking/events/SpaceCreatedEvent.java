package org.boosey.parking.events;

import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;

@ToString
@ApplicationScoped
public class SpaceCreatedEvent extends EventBase<SpaceCreatedEvent> {

    @Inject @Channel("space-created-complete") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "space_events")
    public class SpaceCreatedQueryEventData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public String spaceId;
        public String number;
        public String ownerId;
        public String residentId;
        public Boolean residentApprovedByOwner;
    }

    @PostConstruct
    public void init() {
        this.configure("space-created-complete", concreteEmitter, new SpaceCreatedQueryEventData());
    }

    public SpaceCreatedEvent with(UUID commandId, String spaceId,  String number) {
        SpaceCreatedQueryEventData evtData = (SpaceCreatedQueryEventData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.prepareCommandId(commandId);
        evtData.spaceId = spaceId;
        evtData.number = number;
        return this;
    }
}