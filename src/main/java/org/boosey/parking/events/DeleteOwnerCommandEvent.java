package org.boosey.parking.events;

import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;

@ToString
@ApplicationScoped
public class DeleteOwnerCommandEvent extends EventBase<DeleteOwnerCommandEvent> {

    @Inject @Channel("delete-owner-command") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "owner_events")
    public class DeleteOwnerCommandEventData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public String ownerId;
        public Boolean isIndividualDeletion;
    }

    @PostConstruct
    public void init() {
        this.configure("delete-owner-command", concreteEmitter, new DeleteOwnerCommandEventData());
    }

    public DeleteOwnerCommandEvent with(UUID ownerId, Boolean isIndividualDeletion) {
        DeleteOwnerCommandEventData evtData = (DeleteOwnerCommandEventData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.getNewCommandId().toString();
        evtData.ownerId = ownerId.toString();
        evtData.isIndividualDeletion = isIndividualDeletion;
        return this;
    }
}