package org.boosey.parking.events;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;

@ToString
@ApplicationScoped
public class CreateOwnerCommandEvent extends EventBase<CreateOwnerCommandEvent> {

    @Inject @Channel("create-owner-command") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "owner_events")
    public class CreateOwnerCommandEventData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public String name;
        public String email;
        public String phone;
        public String spaceId;
        public Boolean ownerIsResident;
    }

    @PostConstruct
    public void init() {
        this.configure("create-owner-command", concreteEmitter, new CreateOwnerCommandEventData());
    }

    public CreateOwnerCommandEvent with(String name, String email, String phone, String spaceId, Boolean ownerIsResident) {
        CreateOwnerCommandEventData evtData = (CreateOwnerCommandEventData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.getNewCommandId().toString();
        evtData.name = name;
        evtData.email = email;
        evtData.phone = phone;
        evtData.spaceId = spaceId;
        evtData.ownerIsResident = ownerIsResident;
        return this;
    }
}