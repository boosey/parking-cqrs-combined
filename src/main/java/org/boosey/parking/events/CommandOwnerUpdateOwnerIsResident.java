package org.boosey.parking.events;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;

@ToString
@ApplicationScoped
public class CommandOwnerUpdateOwnerIsResident extends EventBase<CommandOwnerUpdateOwnerIsResident> {

    @Inject @Channel("command-owner-update-owner-is-resident") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "owner_events")
    public class CommandOwnerUpdateOwnerIsResidentData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public String ownerId;
        public Boolean ownerIsResident;
    }

    @PostConstruct
    public void init() {
        this.configure("command-owner-update-owner-is-resident", concreteEmitter, new CommandOwnerUpdateOwnerIsResidentData());
    }

    public CommandOwnerUpdateOwnerIsResident with(String ownerId, Boolean ownerIsResident) {
        CommandOwnerUpdateOwnerIsResidentData evtData = (CommandOwnerUpdateOwnerIsResidentData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.getNewCommandId().toString();
        evtData.ownerId = ownerId;
        evtData.ownerIsResident = ownerIsResident;
        return this;
    }
}