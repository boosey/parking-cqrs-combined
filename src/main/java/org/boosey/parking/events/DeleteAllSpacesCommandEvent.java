package org.boosey.parking.events;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;

@ToString
@ApplicationScoped
public class DeleteAllSpacesCommandEvent extends EventBase<DeleteAllSpacesCommandEvent> {

    @Inject @Channel("delete-all-spaces-command") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "space_events")
    public class DeleteAllSpacesCommandEventData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public Long count;
    }

    @PostConstruct
    public void init() {
        this.configure("delete-all-spaces-command", concreteEmitter, new DeleteAllSpacesCommandEventData());
    }

    public DeleteAllSpacesCommandEvent with(Long count) {
        DeleteAllSpacesCommandEventData evtData = (DeleteAllSpacesCommandEventData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.getNewCommandId().toString();
        evtData.count = count;
        return this;
    }
}