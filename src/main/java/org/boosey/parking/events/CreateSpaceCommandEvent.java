package org.boosey.parking.events;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Channel;
import lombok.ToString;
import org.boosey.parking.events.EventBase;

@ToString
@ApplicationScoped
public class CreateSpaceCommandEvent extends EventBase<CreateSpaceCommandEvent> {

    @Inject @Channel("create-space-command") Emitter<String> concreteEmitter;

    @ToString
    @MongoEntity(collection = "space_events")
    public class CreateSpaceCommandEventData extends PanacheMongoEntity {
        public String eventName;
        public String commandId;
        public String number;
    }

    @PostConstruct
    public void init() {
        this.configure("create-space-command", concreteEmitter, new CreateSpaceCommandEventData());
    }

    public CreateSpaceCommandEvent with(String number) {
        CreateSpaceCommandEventData evtData = (CreateSpaceCommandEventData) this.getEventData();
        evtData.eventName = this.getChannelName();
        evtData.commandId = this.getNewCommandId().toString();
        evtData.number = number;
        return this;
    }
}