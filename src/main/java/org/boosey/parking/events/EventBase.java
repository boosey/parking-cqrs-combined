package org.boosey.parking.events;

import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import com.google.gson.Gson;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.smallrye.reactive.messaging.annotations.Emitter;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import javax.inject.Inject;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.microprofile.context.ThreadContext;

@Slf4j
@ApplicationScoped
public class EventBase<T> {

    @Inject ThreadContext threadContext;
    @Inject ManagedExecutor managedExecutor;
    
    private String channelName;
    private Emitter<String> emitter;
    private PanacheMongoEntity eventData;
    private UUID cmdId;
    protected static Gson gson = new Gson();

    protected void configure(String chnlName, Emitter<String> em,  PanacheMongoEntity evtData) {
        this.channelName = chnlName;
        this.emitter = em;
        this.eventData = evtData;
    }

    protected void runAsync(Runnable fn) {
        managedExecutor.runAsync(threadContext.contextualRunnable(fn));  
    }

    protected UUID getNewCommandId() {
        this.cmdId = UUID.randomUUID();
        return this.cmdId;
    }

    protected String prepareCommandId( UUID commandId) {
        this.cmdId = commandId;
        return commandId.toString();
    }


    public UUID emit() {
        return this.emitIf(true);
    }

    public UUID emitIf(Boolean doEmit) {
        if (doEmit) {
            if (this.cmdId != null) {
                try {
                    this.getEventData().id = null;
                    this.getEventData().persist();
                } catch (Exception e) {
                    log.info("ERROR emitting event");
                    e.printStackTrace();
                }
                this.runAsync(()-> {
                    this.getEmitter().send(gson.toJson(this.getEventData()));
                });

                UUID returnCmdId = this.cmdId;
                this.cmdId = null;
                return returnCmdId;
            } else {
                throw new RuntimeException("Command Id not initialized. Cannot emit event.");
            }
        }
        return null;
    } 

    public Emitter<String> getEmitter() throws RuntimeException {
        if (this.eventData == null) {
            throw new RuntimeException("Emitter not initialized");
        }   
        return this.emitter;
    }

    public String getChannelName() {
        if (this.channelName == null) {
            throw new RuntimeException("Channel Name not initialized");
        }
        return channelName;
    }

    protected PanacheMongoEntity getEventData() {
        if (this.eventData == null) {
            throw new RuntimeException("Event Data not initialized");
        }
        return this.eventData;
    }

    @SuppressWarnings("rawtypes")
    protected Class getEventRecordClass() {
        return this.getEventData().getClass();
    }

    public static String getObjectIdAsString(ObjectId id) {
        return id.toHexString();
    }
}