package org.boosey.parking.owner.rest.api;

import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.gson.JsonObject;
import org.boosey.parking.exceptions.ItemDoesNotExistException;
import org.boosey.parking.exceptions.ItemExistsException;
import org.boosey.parking.servicebase.ParkingRESTApiBase;
import org.boosey.parking.servicebase.ParkingRESTApiInterface;
import org.boosey.parking.owner.commands.OwnerCommands;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.boosey.parking.owner.query.Owner;
import org.boosey.parking.owner.query.OwnerQuery;
import lombok.NonNull;
import lombok.SneakyThrows;

@Path("/owner")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class OwnerRestAPI extends ParkingRESTApiBase<Owner> implements ParkingRESTApiInterface<Owner> {

    @Inject OwnerQuery queryService;
    @Inject OwnerCommands commandService;

    @PostConstruct
    @Override
    public void postConstruct() {
        this.init(queryService, commandService);
    }

    public Owner postCreateCommand(@NonNull Owner s, JsonObject commandResult) {
        s.id = commandResult.get("ownerId").getAsString();
        return s;
    }

    @SneakyThrows(ItemDoesNotExistException.class)
    public UUID setSpaceCommand(String ownerId, String spaceId) {
        return commandService.setSpace(ownerId, spaceId);
    }

    @PUT
    @Path("/{ownerId}/set-space/{spaceId}")
    public Response setSpace(@PathParam("ownerId") String ownerId, @PathParam("spaceId") String spaceId) {
        try {
            this.sendCommand(()-> {
                return this.setSpaceCommand(ownerId, spaceId);
            });
            return Response.ok().build();

        } catch (Exception e) {
            if (e.getClass() == ItemDoesNotExistException.class) {
                return Response.status(Status.NOT_FOUND).build();
            }
            e.printStackTrace();
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        } 
    }

    @SneakyThrows(ItemDoesNotExistException.class)
    public UUID setOwnerIsResidentCommand(String ownerId) {
        return commandService.setOwnerIsResident(ownerId);
    }

    @SneakyThrows(ItemDoesNotExistException.class)
    public UUID clearOwnerIsResidentCommand(String ownerId) {
        return commandService.clearOwnerIsResident(ownerId);
    }

    @PUT
    @Path("/{ownerId}/set-owner-is-resident")
    public Response setOwnerIsResident(@PathParam("ownerId") String ownerId) {
        try {
            this.sendCommand(()-> {
                return this.setOwnerIsResidentCommand(ownerId);
            });
            return Response.ok().build();

        } catch (Exception e) {
            if (e.getClass() == ItemDoesNotExistException.class) {
                return Response.status(Status.NOT_FOUND).build();
            }
            e.printStackTrace();
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        } 
    }

    @PUT
    @Path("/{ownerId}/clear-owner-is-resident")
    public Response clearOwnerIsResident(@PathParam("ownerId") String ownerId) {
        try {
            this.sendCommand(()-> {
                return this.clearOwnerIsResidentCommand(ownerId);
            });

            return Response.ok().build();

        } catch (Exception e) {

            if (e.getClass() == ItemDoesNotExistException.class) {
                return Response.status(Status.NOT_FOUND).build();
            }
            e.printStackTrace();
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        } 
    }

    @SneakyThrows(ItemExistsException.class)
    public UUID createCommand(@NonNull Owner o) {
        return commandService.create(o);
    }

    @SneakyThrows(ItemDoesNotExistException.class)
    public UUID updateDetailsCommand(@NonNull String ownerId, @NonNull Owner o) {
        return commandService.updateDetails(ownerId, o);
    }

    @Incoming("owner-created-complete-incoming")
    public void handleOwnerCreatedComplete(String evt) {
        completeCommand(evt);
    }

    @Incoming("complete-owner-update-details-incoming")
    public void handleCompleteOwnerUpdateDetails(String evt) {
        completeCommand(evt);
    }

    @Incoming("complete-owner-update-space-incoming")
    public void handleCompleteOwnerUpdateSpace(String evt) {
        completeCommand(evt);
    }

    @Incoming("complete-owner-update-owner-is-resident-incoming")
    public void handleCompleteOwnerUpdateOwnerIsResident(String evt) {
        completeCommand(evt);
    }

    @Incoming("owner-all-deleted-complete-incoming")
    public void handleAllOwnersDeletedComplete(String evt) {
        completeCommand(evt);
    }

    @Incoming("owner-deleted-complete-incoming")
    public void handleOwnerDeletedComplete(String evt) {
        completeCommand(evt);
    }
}