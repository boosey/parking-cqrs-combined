package org.boosey.parking.owner.query;

import org.boosey.parking.events.CompleteOwnerUpdateDetails;
import org.boosey.parking.events.CompleteOwnerUpdateOwnerIsResident;
import org.boosey.parking.events.CompleteOwnerUpdateSpace;
import org.boosey.parking.events.OwnerAllDeletedCompleteEvent;
import org.boosey.parking.events.OwnerCreatedCompleteEvent;
import org.boosey.parking.events.OwnerDeletedCompleteEvent;
import org.boosey.parking.events.CommandOwnerUpdateDetails.CommandOwnerUpdateDetailsData;
import org.boosey.parking.events.CommandOwnerUpdateOwnerIsResident.CommandOwnerUpdateOwnerIsResidentData;
import org.boosey.parking.events.CommandOwnerUpdateSpace.CommandOwnerUpdateSpaceData;
import org.boosey.parking.events.CreateOwnerCommandEvent.CreateOwnerCommandEventData;
import org.boosey.parking.events.DeleteAllOwnersCommandEvent.DeleteAllOwnersCommandEventData;
import org.boosey.parking.events.DeleteOwnerCommandEvent.DeleteOwnerCommandEventData;
import org.boosey.parking.exceptions.ItemDoesNotExistException;
import org.boosey.parking.servicebase.QueryServiceBase;
import org.boosey.parking.servicebase.QueryServiceInterface;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.UUID;

@Slf4j
@ApplicationScoped
public class OwnerQuery extends QueryServiceBase<Owner> implements QueryServiceInterface<Owner> {

	@Inject OwnerCreatedCompleteEvent ownerCreatedComplete;
    @Inject OwnerDeletedCompleteEvent ownerDeletedComplete;
    @Inject OwnerAllDeletedCompleteEvent ownerAllDeletedComplete;
    @Inject CompleteOwnerUpdateDetails completeOwnerUpdateDetails;
    @Inject CompleteOwnerUpdateSpace completeOwnerUpdateSpace;
    @Inject CompleteOwnerUpdateOwnerIsResident completeOwnerUpdateOwnerIsResident;

    public OwnerQuery() { }

    @PostConstruct
    public void init() {
        this.configure(Owner.class);
    }

    public Boolean exists(Owner o) {
        return Owner.exists(o);
    }

    @Incoming("create-owner-command-incoming")
    public void handleOwnerCreatedCommand(String evt) {
        CreateOwnerCommandEventData evtData = this.getEventData(evt, CreateOwnerCommandEventData.class);
        Owner ownr = new Owner();
        ownr.id = UUID.randomUUID().toString();
        ownr.name = evtData.name;
        ownr.email = evtData.email;
        ownr.phone = evtData.phone;
        ownr.spaceId = evtData.spaceId;
        ownr.ownerIsResident = evtData.ownerIsResident;

        executeTransaction(() -> {
            ownr.persistAndFlush();
            return true;
        });    
        ownerCreatedComplete.with(UUID.fromString(evtData.commandId), ownr.id).emit();
    }

    @SneakyThrows(ItemDoesNotExistException.class)
    protected Owner getOwnerById(String ownerId) {
       return this.getById(ownerId);
    }

    protected <T> T optionalUpdate(T newValue, T defaultValue) {
        return newValue != null ? newValue : defaultValue;
    }

    @Incoming("command-owner-update-details-incoming")
    public void handleCommandOwnerUpdateDetails(String evt) {

        CommandOwnerUpdateDetailsData evtData = this.getEventData(evt, CommandOwnerUpdateDetailsData.class);
        try {
            executeTransaction(()->{
                // TODO: Refactor into base class
                Owner o = this.getOwnerById(evtData.ownerId);
                o.name = optionalUpdate(evtData.name, o.name);
                o.email = optionalUpdate(evtData.email, o.email); 
                o.phone = optionalUpdate(evtData.phone, o.phone); 
                o.persistAndFlush(); 
                
                completeOwnerUpdateDetails.with(UUID.fromString(evtData.commandId), o.id, o.name, o.email, o.phone).emit();
                return true;
            });
            
		} catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		}

    }

    @Incoming("command-owner-update-space-incoming")
    public void handleCommandOwnerUpdateSpace(String evt) {

        CommandOwnerUpdateSpaceData evtData = this.getEventData(evt, CommandOwnerUpdateSpaceData.class);
        try {
            log.info("query processing: {}", evtData);
            executeTransaction(()->{
                // TODO: Refactor into base class
                Owner o = this.getOwnerById(evtData.ownerId);
                o.spaceId = optionalUpdate(evtData.spaceId, o.spaceId);
                o.persistAndFlush(); 
                
                completeOwnerUpdateSpace.with(UUID.fromString(evtData.commandId), evtData.ownerId, evtData.spaceId).emit();
                return true;
            });
            
		} catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		}

    }

    @Incoming("command-owner-update-owner-is-resident-incoming")
    public void handleCommandOwnerUpdateOwnerIsResident(String evt) {

        CommandOwnerUpdateOwnerIsResidentData evtData = this.getEventData(evt, CommandOwnerUpdateOwnerIsResidentData.class);
        try {
            log.info("query processing: {}", evtData);
            executeTransaction(()->{
                // TODO: Refactor into base class
                Owner o = this.getOwnerById(evtData.ownerId);
                o.ownerIsResident = evtData.ownerIsResident;
                o.persistAndFlush(); 
                
                completeOwnerUpdateSpace.with(UUID.fromString(evtData.commandId), evtData.ownerId, 
                        evtData.ownerIsResident.toString()).emit();

                return true;
            });
            
		} catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
		}

    }

    @Incoming("delete-all-owners-command-incoming")
    public void handleAllOwnersDeletedCommand(String evt) {
        DeleteAllOwnersCommandEventData evtData = this.getEventData(evt, DeleteAllOwnersCommandEventData.class);
        executeTransaction(() -> {
            Owner.deleteAll();
            return true;
        });    
        ownerAllDeletedComplete.with(UUID.fromString(evtData.commandId), evtData.count).emit();
    }

    @Incoming("delete-owner-command-incoming")
    public void handleOwnerDeletedCommand(String evt) {
        DeleteOwnerCommandEventData evtData = this.getEventData(evt, DeleteOwnerCommandEventData.class);
        if(evtData.isIndividualDeletion) {
            executeTransaction(() -> {
                Owner ownr = null;
				try {
					ownr = this.getById(evtData.ownerId);
				} catch (ItemDoesNotExistException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
				}
                ownr.delete();
                return true;
            });    
        }
        ownerDeletedComplete.with(UUID.fromString(evtData.commandId), UUID.fromString(evtData.ownerId), evtData.isIndividualDeletion).emit();
    }
}