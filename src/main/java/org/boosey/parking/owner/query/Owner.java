package org.boosey.parking.owner.query;

import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Entity;
import javax.persistence.Id;
import org.boosey.parking.servicebase.ParkingEntityBase;
import lombok.ToString;

@ToString
@Entity
@ApplicationScoped
public class Owner extends ParkingEntityBase {

    @Id
    public String id;

    public String name;
    public String email;
    public String phone;
    public String spaceId;
    public Boolean ownerIsResident;

    public String idAString() {
        return this.id;
    }

    public static Owner findByEmail(String email) {
        return find("email", email).firstResult();
    }

    public static Optional<Owner> findByEmailOptional(String email){
        return Optional.ofNullable(findByEmail(email));
    }

    public static Boolean exists(Owner o) {
        if (o.email == null) {
            throw new RuntimeException("Owner email is not set");
        }
        return Owner.findByEmailOptional(o.email).isPresent();
    }
    
    public static Boolean doesNotExist(Owner o) {
        return !Owner.exists(o);
    }
}