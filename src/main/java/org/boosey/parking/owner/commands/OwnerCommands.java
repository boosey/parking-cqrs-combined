package org.boosey.parking.owner.commands;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import com.google.gson.Gson;
import org.boosey.parking.events.CommandOwnerUpdateDetails;
import org.boosey.parking.events.CommandOwnerUpdateOwnerIsResident;
import org.boosey.parking.events.CommandOwnerUpdateSpace;
import org.boosey.parking.events.CreateOwnerCommandEvent;
import org.boosey.parking.events.DeleteAllOwnersCommandEvent;
import org.boosey.parking.events.DeleteOwnerCommandEvent;
import org.boosey.parking.events.EventBase;
import org.boosey.parking.exceptions.ItemDoesNotExistException;
import org.boosey.parking.exceptions.ItemExistsException;
import org.boosey.parking.servicebase.CommandServiceBase;
import org.boosey.parking.servicebase.CommandServiceInterface;
import lombok.NonNull;
import org.boosey.parking.owner.query.Owner;
import org.boosey.parking.owner.query.OwnerQuery;

@ApplicationScoped
public class OwnerCommands extends CommandServiceBase<Owner> implements CommandServiceInterface<Owner> {

    @Inject OwnerQuery ownerQuery;
    @Inject CreateOwnerCommandEvent createOwnerCommandEvent;
    @Inject DeleteAllOwnersCommandEvent deleteAllOwnersCommandEvent;
    @Inject DeleteOwnerCommandEvent deleteOwnerCommandEvent;
    @Inject CommandOwnerUpdateDetails commandOwnerUpdateDetails;
    @Inject CommandOwnerUpdateSpace commandOwnerUpdateSpace;
    @Inject CommandOwnerUpdateOwnerIsResident commandOwnerUpdateOwnerIsResident;
    @Inject Owner owner;

    static final Gson gson = new Gson();

    @PostConstruct
    public void init() {
        this.configure(ownerQuery);
    }

    public UUID deleteAll() {
        return this.deleteAll(deleteAllOwnersCommandEvent.with(Owner.count()));
    }

    // Each subclass must implement this method in order to pass in the provide the specific event to emit
    public UUID delete(UUID ownerId, Boolean isIndividualDeletion) throws ItemDoesNotExistException {
        // Call the ultimate method which is in the super class passing the specific event to emit
        return this.delete(ownerId, isIndividualDeletion, deleteOwnerCommandEvent.with(ownerId, isIndividualDeletion)); 
    }

    public UUID create(Owner o) throws ItemExistsException {
        // If Owner exists throw an exception
        if (ownerQuery.exists(o))
            throw new ItemExistsException();

        return createOwnerCommandEvent.with(o.name, o.email, o.phone, o.spaceId, o.ownerIsResident).emit();    
    
    }

    protected UUID update(String id, EventBase<Owner> evt) throws ItemDoesNotExistException {

        AtomicReference<UUID> uuid = new AtomicReference<>();
        this.getQueryService().getByIdOptional(id)
            .map((o) -> {
                uuid.set(evt.emit());
                return o;
            })
            .orElseThrow((() -> {
                return new ItemDoesNotExistException();
             }));

        return uuid.get();        
    }

    public UUID updateDetails(@NonNull String ownerId, @NonNull Owner owner) throws ItemDoesNotExistException {
        return this.updateDetails(ownerId, commandOwnerUpdateDetails.with(ownerId, owner.name, owner.email, owner.phone));
    }

    public UUID setSpace(@NonNull String ownerId, String spaceId) throws ItemDoesNotExistException  {

        AtomicReference<UUID> uuid = new AtomicReference<>();
        ownerQuery.getByIdOptional(ownerId)
            .map((o) -> {
                uuid.set(commandOwnerUpdateSpace.with(ownerId, spaceId).emit());
                return o;
            })
            .orElseThrow((() -> {
                return new ItemDoesNotExistException();
             }));

        return uuid.get();
    }

    public UUID setOwnerIsResident(@NonNull String ownerId) throws ItemDoesNotExistException {
        return this.updateOwnerOwnerIsResident(ownerId, true);
    }

    public UUID clearOwnerIsResident(@NonNull String ownerId) throws ItemDoesNotExistException {
        return this.updateOwnerOwnerIsResident(ownerId, false);
    }

    public UUID updateOwnerOwnerIsResident(@NonNull String ownerId, Boolean ownerIsResident) throws ItemDoesNotExistException  {

        AtomicReference<UUID> uuid = new AtomicReference<>();
        ownerQuery.getByIdOptional(ownerId)
            .map((o) -> {
                uuid.set(commandOwnerUpdateOwnerIsResident.with(ownerId, ownerIsResident).emit());
                return o;
            })
            .orElseThrow((() -> {
                return new ItemDoesNotExistException();
             }));

        return uuid.get();
    }
}