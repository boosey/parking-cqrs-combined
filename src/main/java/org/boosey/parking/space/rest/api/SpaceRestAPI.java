package org.boosey.parking.space.rest.api;

import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.google.gson.JsonObject;
import org.boosey.parking.exceptions.ItemExistsException;
import org.boosey.parking.exceptions.ItemNotAcceptedException;
import org.boosey.parking.servicebase.ParkingRESTApiBase;
import org.boosey.parking.servicebase.ParkingRESTApiInterface;
import org.boosey.parking.space.commands.SpaceCommands;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.boosey.parking.space.query.Space;
import org.boosey.parking.space.query.SpaceQuery;
import lombok.NonNull;
import lombok.SneakyThrows;

@Path("/space")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class SpaceRestAPI extends ParkingRESTApiBase<Space> implements ParkingRESTApiInterface<Space> {

    @Inject SpaceQuery queryService;
    @Inject SpaceCommands commandService;

    @PostConstruct
    @Override
    public void postConstruct() {
        this.init(queryService, commandService);
    }

    @SneakyThrows(ItemExistsException.class)
    public UUID createCommand(@NonNull Space s) {
        return commandService.create(s);
    }

    public Space preCreateCommand(Space s) throws ItemExistsException, ItemNotAcceptedException {
        if (s.number == null) {
            throw new ItemNotAcceptedException("No space number provided");
        }
        if (Space.spaceNumberExists(s.number)) {
            throw new ItemExistsException();
        }
        return s;
    }

    public Space postCreateCommand(@NonNull Space s, JsonObject commandResult) {
        s.id = commandResult.get("spaceId").getAsString();
        return s;
    }

    @Incoming("space-created-complete-incoming")
    public void handleSpaceCreatedComplete(String evt) {
        completeCommand(evt);
    }

    @Incoming("space-all-deleted-complete-incoming")
    public void handleAllSpacesDeletedComplete(String evt) {
        completeCommand(evt);
    }

    @Incoming("space-deleted-complete-incoming")
    public void handleSpaceDeletedComplete(String evt) {
        completeCommand(evt);
    }
}