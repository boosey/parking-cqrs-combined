package org.boosey.parking.space.commands;

import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import com.google.gson.Gson;
import org.boosey.parking.events.CreateSpaceCommandEvent;
import org.boosey.parking.events.DeleteAllSpacesCommandEvent;
import org.boosey.parking.events.DeleteSpaceCommandEvent;
import org.boosey.parking.exceptions.ItemDoesNotExistException;
import org.boosey.parking.exceptions.ItemExistsException;
import org.boosey.parking.servicebase.CommandServiceBase;
import org.boosey.parking.servicebase.CommandServiceInterface;
import org.boosey.parking.space.query.Space;
import org.boosey.parking.space.query.SpaceQuery;

@ApplicationScoped
public class SpaceCommands extends CommandServiceBase<Space> implements CommandServiceInterface<Space> {

    @Inject SpaceQuery spaceQuery;
    @Inject CreateSpaceCommandEvent createSpaceCommandEvent;
    @Inject DeleteAllSpacesCommandEvent deleteAllSpacesCommandEvent;
    @Inject DeleteSpaceCommandEvent deleteSpaceCommandEvent;
    @Inject Space space;

    static final Gson gson = new Gson();

    @PostConstruct
    public void init() {
        this.configure(spaceQuery);
    }

    public UUID deleteAll() {
        return this.deleteAll(deleteAllSpacesCommandEvent.with(Space.count()));
    }

    // Each subclass must implement this method in order to pass in the provide the specific event to emit
    public UUID delete(UUID spaceId, Boolean isIndividualDeletion) throws ItemDoesNotExistException {
        // Call the ultimate method which is in the super class passing the specific event to emit
        return this.delete(spaceId, isIndividualDeletion, deleteSpaceCommandEvent.with(spaceId, isIndividualDeletion)); 
    }

    public UUID create(Space s) throws ItemExistsException {
        if (spaceQuery.exists(s))
            throw new ItemExistsException();

        return createSpaceCommandEvent.with(s.number).emit();    
    }

    // // @EVENT
    // public void handleOwnerDeleted(OwnerDeletedEvent e) {
    //     try {
    //         Optional<Space> o_spc = Space.find("ownerId", e.ownerId).firstResultOptional();
    //         o_spc.orElseThrow(SpaceDoesNotExistException::new);

    //         Space spc = o_spc.get();
    //         Space spc_orig = o_spc.get();

    //         spc.ownerId = null;
    //         spc.residentId = null;
    //         spc.residentApprovedByOwner = false;
    //         // spc.update();
    //         spc.persist();

    //         // SpaceOwnerRemovedEvent.with(spc.id).emit();

    //         // Only emit if resident was orginally set
    //         // SpaceResidentRemovedEvent.with(spc.id).emitIf(spc_orig.residentId != null);

    //         // Only emit if approved previously; With new owner the resident must be re-associated and re-approved
    //         // SpaceResidentApprovalRemovedEvent.with(spc.id).emitIf(spc_orig.residentApprovedByOwner);

    //     } catch (Exception ex) {
    //         log.error("Error processing owner-deleted", ex);   
    //     }
    // }

    // // @EVENT
    // public void handleOwnerCreated(OwnerCreatedEvent e) {
    //     try {
    //         log.info("Owner created for space {}", e.spaceId);
    //         // Optional<Space> o_spc = Space.findByIdOptional(new ObjectId(e.spaceId));
    //         Optional<Space> o_spc = Space.findByIdOptional(e.spaceId);
    //         o_spc.orElseThrow(SpaceDoesNotExistException::new);

    //         // Change owner on existing space
    //         Space spc_orig = o_spc.get();
    //         Space spc = o_spc.get();

    //         spc.ownerId = e.ownerId;
    //         // When new Owner is created the Space's resident must be set and approved again
    //         spc.residentId = null;
    //         spc.residentApprovedByOwner = false;

    //         spc.persist();

    //         // SpaceOwnerRemovedEvent.with(spc.id).emitIf(spc_orig.ownerId != null);
    //         // SpaceOwnerSetEvent.with(spc.id, spc.ownerId);

    //         // Only emit if approved previously; With new owner the resident must be re-associated and re-approved
    //         // SpaceResidentRemovedEvent.with(spc.id).emitIf(spc_orig.residentId != null);
    //         // SpaceResidentApprovalRemovedEvent.with(spc.id).emitIf(spc_orig.residentApprovedByOwner);

    //     } catch (Exception ex) {
    //         log.error("Error processing owner-created", ex);   
    //     }
    // }
}