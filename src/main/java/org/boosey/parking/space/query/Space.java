package org.boosey.parking.space.query;

import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import org.boosey.parking.servicebase.ParkingEntityBase;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.ToString;

@ToString
@ApplicationScoped
@RegisterForReflection 
@Entity
public class Space extends ParkingEntityBase {

    @Id
    public String id;

    @Column(unique = true)
    public String number;
    
    public String ownerId;
    public String residentId;
    public Boolean residentApprovedByOwner;

    public String idAsString() {
        return this.id;
    }

    public static Space findByNumber(String number) {
        return find("number", number).firstResult();
    }

    public static Optional<Space> findByNumberOptional(String number){
        return Optional.ofNullable(findByNumber(number));
    }

    public static Boolean spaceNumberExists(String number) {
        return Space.findByNumberOptional(number).isPresent();
    }

    public static Boolean exists(Space o) {
        if (o.number == null) {
            throw new RuntimeException("Owner email is not set");
        }
        return Space.findByNumberOptional(o.number).isPresent();
    }
    
    public static Boolean doesNotExist(Space o) {
        return !Space.exists(o);
    }
}