package org.boosey.parking.space.query;

import java.util.Optional;
import org.boosey.parking.events.SpaceAllDeletedEvent;
import org.boosey.parking.events.SpaceCreatedEvent;
import org.boosey.parking.events.SpaceDeletedEvent;
import org.boosey.parking.events.CreateSpaceCommandEvent.CreateSpaceCommandEventData;
import org.boosey.parking.events.DeleteAllSpacesCommandEvent.DeleteAllSpacesCommandEventData;
import org.boosey.parking.events.DeleteSpaceCommandEvent.DeleteSpaceCommandEventData;
import org.boosey.parking.exceptions.ItemDoesNotExistException;
import org.boosey.parking.servicebase.QueryServiceBase;
import org.boosey.parking.servicebase.QueryServiceInterface;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.UUID;

@ApplicationScoped
public class SpaceQuery extends QueryServiceBase<Space> implements QueryServiceInterface<Space> {

    @Inject SpaceCreatedEvent spaceCreatedComplete;
    @Inject SpaceDeletedEvent spaceDeletedComplete;
    @Inject SpaceAllDeletedEvent spaceAllDeletedComplete;

    public SpaceQuery() { }

    @PostConstruct
    public void init() {
        this.configure(Space.class);
    }

    public Optional<Space> findByNumberOptional(String number) {
        return Space.findByNumberOptional(number);
    }

    public Boolean spaceNumberExists(String number) {
        return this.findByNumberOptional(number).isPresent();
    }

    public Boolean spaceNumberDoesNotExist(String number) {
        return !(this.spaceNumberExists(number));
    }

    public Boolean exists(Space s) {
        return Space.exists(s);
    }

    @Incoming("create-space-command-incoming")
    public void handleSpaceCreatedCommand(String evt) {
        CreateSpaceCommandEventData evtData = this.getEventData(evt, CreateSpaceCommandEventData.class);
        Space spc = new Space();
        spc.id = UUID.randomUUID().toString();
        spc.number = evtData.number;

        executeTransaction(() -> {
            spc.persistAndFlush();
            return true;
        });    
        spaceCreatedComplete.with(UUID.fromString(evtData.commandId), spc.id, spc.number).emit();
    }

    @Incoming("delete-all-spaces-command-incoming")
    public void handleAllSpacesDeletedCommand(String evt) {
        DeleteAllSpacesCommandEventData evtData = this.getEventData(evt, DeleteAllSpacesCommandEventData.class);
        executeTransaction(() -> {
            Space.deleteAll();
            return true;
        });    
        spaceAllDeletedComplete.with(UUID.fromString(evtData.commandId), evtData.count).emit();
    }

    @Incoming("delete-space-command-incoming")
    public void handleSpaceDeletedCommand(String evt) {
        DeleteSpaceCommandEventData evtData = this.getEventData(evt, DeleteSpaceCommandEventData.class);
        if(evtData.isIndividualDeletion) {
            executeTransaction(() -> {
                Space spc = null;
				try {
					spc = this.getById(evtData.spaceId);
				} catch (ItemDoesNotExistException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
				}
                spc.delete();
                return true;
            });    
        }
        spaceDeletedComplete.with(UUID.fromString(evtData.commandId), UUID.fromString(evtData.spaceId), evtData.isIndividualDeletion).emit();
    }
}