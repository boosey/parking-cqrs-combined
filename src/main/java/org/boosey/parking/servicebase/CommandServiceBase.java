package org.boosey.parking.servicebase;

import java.lang.reflect.Field;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

import org.boosey.parking.events.EventBase;
import org.boosey.parking.exceptions.ItemDoesNotExistException;
import org.boosey.parking.exceptions.ItemExistsException;
import lombok.NonNull;

public class CommandServiceBase<T extends ParkingEntityBase> extends ServiceBase {

    QueryServiceBase<T> queryService;

    public void configure(QueryServiceBase<T> queryService) {
        this.queryService = queryService;
    }

    public QueryServiceBase<T> getQueryService() {
        if (queryService == null)
            throw new RuntimeException("getQueryService() must be implemented by subclass");

        return queryService;
    }

    public T updateFromNonNullFields(@NonNull T dst, @NonNull T src) {
        for (Field f : dst.getClass().getDeclaredFields()) {
            try {
                if(f.get(src) != null) {
                    f.setAccessible(true);
                    f.set(dst, f.get(src)); 
                }
            } catch (Exception e) {
                throw new RuntimeException("Error copying object fields", e);    
            }
        }
        return dst;
    } 

    public UUID create(@NonNull T t) throws ItemExistsException {
        throw new RuntimeException("The sublcass should always implement create()");
    }

    public UUID updateDetails(@NonNull String itemId, EventBase<?> evt) throws ItemDoesNotExistException {

        AtomicReference<UUID> uuid = new AtomicReference<>();

        this.getQueryService().getByIdOptional(itemId)
            .map((o) -> {
                uuid.set(evt.emit());
                return o;
            })
            .orElseThrow((() -> {
                return new ItemDoesNotExistException();
             }));

        return uuid.get();
    }

    public UUID deleteAll() {
        throw new RuntimeException("The sublcass should always implement deleteAll()");
    }

    public UUID deleteAll(EventBase<?> evt) {
        @SuppressWarnings("unchecked")
        List<T> itemList = (List<T>)this.getQueryService().listAll();
        UUID cmdId = evt.emit();

        this.runAsync(()-> {
           itemList.forEach( (s) -> {
                try { 
					this.delete(UUID.fromString(s.idAsString()), false);
				} catch (ItemDoesNotExistException e) {
                    e.printStackTrace();
                    throw new RuntimeException("Should never try to check for existence when doing a mass delete", e);
				}
            }); 
        });
        
        return cmdId;
    }

    public UUID delete(UUID itemId, Boolean isIndividualDeletion, EventBase<?> evt) throws ItemDoesNotExistException {
        // If this is not a part of a mass deletion, then we can just send the notification
        // If it is a deletion of an invidual item check the the item exist
        if (isIndividualDeletion && !(this.getQueryService().getByIdOptional(itemId).isPresent()))
            throw new ItemDoesNotExistException();

        return evt.emit(); 
    }

    // Convenience method for individual deletion
    public UUID delete(UUID id) throws ItemDoesNotExistException {
        return delete(id, true);   
    }

    public UUID delete(UUID id, Boolean isIndividualDeletion) throws ItemDoesNotExistException {
        throw new RuntimeException("delete(UUID id, Boolean isIndividualDeletion) must be implemented in a subslcass");
    }
}