package org.boosey.parking.servicebase;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.boosey.parking.exceptions.ItemDoesNotExistException;

import lombok.NonNull;

public interface QueryServiceInterface<T> {
    public void init();
    public List<?> listAll();
    public Optional<?> getByIdOptional(@NonNull String id);
    public T getById(@NonNull String id) throws ItemDoesNotExistException;
    public T getById(@NonNull UUID id) throws ItemDoesNotExistException;
    public Optional<T> getByIdOptional(@NonNull UUID id);
    public Boolean exists(T t);
    public Boolean doesNotExist(T t);
    
}