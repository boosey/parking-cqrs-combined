package org.boosey.parking.servicebase;

import java.util.UUID;
import org.boosey.parking.exceptions.ItemDoesNotExistException;
import org.boosey.parking.exceptions.ItemExistsException;

import lombok.NonNull;

public interface CommandServiceInterface<T extends ParkingEntityBase> {
    public void init();
    public QueryServiceBase<?> getQueryService();
    public UUID deleteAll();
    public UUID delete(UUID id, Boolean isIndividualDeletion) throws ItemDoesNotExistException;
    public UUID create(@NonNull T t) throws ItemExistsException;
}