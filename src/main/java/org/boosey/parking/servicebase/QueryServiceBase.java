package org.boosey.parking.servicebase;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.boosey.parking.exceptions.ItemDoesNotExistException;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.NonNull;

public class QueryServiceBase<T extends PanacheEntityBase> extends ServiceBase {

    Class<T> clazz;

    public void configure(Class<T> clazz) {
        this.clazz =  clazz;
    }

    public Boolean exists(T t) {
        throw new RuntimeException("exists() must be implemented in a QueryServiceBase subclass");
    }

    public Boolean doesNotExist(T t) {
        return !this.exists(t);
    }

    public List<?> listAll() {
        List<?> returnList = null;
        Method listAll;
        try {
            listAll =  clazz.getDeclaredMethod("listAll"); 
            returnList = (List<?>)listAll.invoke(null); 
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnList;
    } 

    public Optional<T> getByIdOptional(@NonNull String id) {
        Optional<T> r = null;
        Method m;
        try {
            m =  clazz.getDeclaredMethod("findByIdOptional", Object.class); 
            return (Optional<T>) m.invoke(null, id); 

        } catch (Exception e) {
            e.printStackTrace();
        }
        return r;
    }

    public T getById(@NonNull String id) throws ItemDoesNotExistException {
        T r = (T) this.getByIdOptional(id).orElseThrow(ItemDoesNotExistException::new);
        return r;
    }
    
    public T getById(@NonNull UUID id) throws ItemDoesNotExistException {
        return this.getById(id.toString());
    }

    public Optional<T> getByIdOptional(@NonNull UUID id) {
        Optional<T> r = (Optional<T>) this.getByIdOptional(id.toString());
        return r;
    }
}