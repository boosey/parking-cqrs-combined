package org.boosey.parking.servicebase;

import java.util.UUID;

import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;

import org.boosey.parking.exceptions.ItemExistsException;
import org.boosey.parking.exceptions.ItemNotAcceptedException;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

public interface ParkingRESTApiInterface<T extends PanacheEntityBase> {
    public void init(QueryServiceBase<?> q, CommandServiceBase<?> c);
    public void postConstruct();
    public QueryServiceBase<?> getQueryService();
    public CommandServiceBase<?> getCommandService();
    public Response create(T t);
    public JsonObject sendCreateCommand(T t);
    public T preCreateCommand(T t) throws ItemExistsException, ItemNotAcceptedException;
    public T postCreateCommand(T t, JsonObject commandResult);
    public UUID createCommand(T t);
    public UUID updateDetailsCommand(String itemId, T t);
}
