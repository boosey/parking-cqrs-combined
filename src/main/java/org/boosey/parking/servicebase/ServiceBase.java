package org.boosey.parking.servicebase;

import java.util.UUID;
import java.util.function.Supplier;
import javax.inject.Inject;
import javax.transaction.UserTransaction;

import com.google.gson.Gson;

import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.microprofile.context.ThreadContext;

public class ServiceBase {

    @Inject ThreadContext threadContext;
    @Inject ManagedExecutor managedExecutor;
    @Inject UserTransaction transaction;

    static Gson gson = new Gson();

    protected UUID getNewCommandId() {
        return UUID.randomUUID();
    }

    protected <T> void runAsync(Runnable fn) {
        managedExecutor.runAsync(threadContext.contextualRunnable(fn));  
    }

    protected Boolean executeTransaction(Supplier<Boolean> tx) {

        managedExecutor.runAsync(threadContext.contextualRunnable( () -> {

            try {
                transaction.begin();
                tx.get();
                transaction.commit();

            } catch (Exception e) {
                try {
                    transaction.rollback();
                    e.printStackTrace();
                } catch (Exception e1) {
                    e1.printStackTrace();
                } 
            } 
        }));

        return true;
    }

    protected <T> T getEventData(String evtString, Class<T> clazz) {
        return gson.fromJson(evtString, clazz);
    }
}