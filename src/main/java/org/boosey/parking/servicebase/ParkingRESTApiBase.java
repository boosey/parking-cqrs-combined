package org.boosey.parking.servicebase;

import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.reactivex.subjects.UnicastSubject;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.boosey.parking.configuration.ParkingConfiguration;
import org.boosey.parking.exceptions.ItemDoesNotExistException;
import org.boosey.parking.exceptions.ItemExistsException;
import org.boosey.parking.exceptions.ItemNotAcceptedException;

@Slf4j
public class ParkingRESTApiBase<T extends PanacheEntityBase> {

    @Inject ParkingConfiguration parkingConfiguration;
    
    protected final Gson gson = new Gson();
    static HashMap<UUID, UnicastSubject<JsonObject>> commandCompletioMap = new HashMap<>();
    private QueryServiceBase<?> queryService;
    private CommandServiceBase<?> commandService;

    public void init(QueryServiceBase<?> q, CommandServiceBase<?> c) {
        this.queryService = q;
        this.commandService = c;
    }

    public void postConstruct() {
        throw new RuntimeException("A subclass must implement the postConstruct message to set the queryService and the commandService");
    }

    public QueryServiceBase<?> getQueryService() {
        return queryService;
    }

    public CommandServiceBase<?> getCommandService() {
        return commandService;
    }
    
    protected Optional<JsonObject> sendCommand(Supplier<UUID> cmd) { 
        return this.sendCommandWithTimeout(cmd, parkingConfiguration.getTimeout());
    }

    protected Optional<JsonObject> sendCommandWithTimeout(Supplier<UUID> cmd, Long timeout) {
            // Call the command; Create a completion Subject and Block on it until a completion message is received
            // The block is removed when the message handler sends a JsonObject to the Subject which is returned for processing
            // If the completion evtData is not emitted within the timeout period a TimeoutException is thrown

            Optional<UUID> cmdId = Optional.ofNullable(cmd.get());

            Optional<JsonObject> completionData = cmdId.map((cId) -> {
                UnicastSubject<JsonObject> sub = UnicastSubject.create();
                commandCompletioMap.put(cmdId.get(), sub);

                return sub
                    .timeout(timeout, TimeUnit.MILLISECONDS)
                    .blockingFirst(); 
            });

            return completionData;
    }

    protected Optional<JsonObject> getEventData(String evt) {
        JsonObject eData = null;
        try {
            eData = gson.fromJson(evt, JsonObject.class);
        } catch (Exception e) {
            throw new RuntimeException("Unable to create JsonObject from event string", e);
        }
        return Optional.ofNullable(eData);
    }

    protected Optional<UUID> getCommandId(JsonObject evtData) {
        UUID cmdId = null;
        try {
            cmdId = UUID.fromString(evtData.get("commandId").getAsString());
        } catch (Exception e) {
            throw new RuntimeException("Unable to commandId from event event data", e);
        }
        return Optional.ofNullable(cmdId);
    }

    protected Boolean getBooleanField(JsonObject evtData, String field) {
        Boolean b = false;
        try {
            b = evtData.get("commandId").getAsBoolean();
        } catch (Exception e) {
            throw new RuntimeException("Unable to commandId from event event data", e);
        }
        return b;
    }

    protected void completeCommand(String evt) {
        getEventData(evt)
            .ifPresent((eData) -> fireSubject(eData));
    }

    protected void fireSubject(JsonObject eData) {
        getCommandId(eData).ifPresent((cmdId) -> {
            Optional.ofNullable(commandCompletioMap.get(cmdId)).ifPresent((sub) -> {
                sub.onNext(eData);
                sub.onComplete();   
            });
        });
    }

    public T preCreateCommand(T t) throws ItemExistsException, ItemNotAcceptedException {
        return t;
    }

    public T postCreateCommand(T t, JsonObject commandResult) {
        throw new RuntimeException("sendCreateCommand() must be implemented by a subclass");
    }

    public UUID createCommand(T t) {
        throw new RuntimeException("createCommand() must be implemented by a subclass");
    }

    public UUID updateDetailsCommand(String id, T t) {
        throw new RuntimeException("updateDetailsCommand() must be implemented by a subclass");
    }

    public JsonObject sendCreateCommand(@NonNull T t) {

        Supplier<UUID> supplierOfCreateCommand = ()->{
            return this.createCommand(t);
        };

        return this.sendCommand(supplierOfCreateCommand).get();
    }
 
    @POST
    public Response create(T t) {
        try {      
            T t1 = this.preCreateCommand(t);
            JsonObject resultJSON = this.sendCreateCommand(t1);
            T t2 = this.postCreateCommand(t1, resultJSON);
            return Response.status(Status.CREATED).entity(t2).build();

        } catch (Exception e) {
            // Using SneakyThrows on createCommand to simplify the lamba in sendCommand
            // Otherwise the lambda needs a try-catch inside of it
            // SneakyThrows the original exception as a generic Exception
            // So, we must check for the specific class of error to provide the proper response

            if (e.getClass() == ItemExistsException.class) {
                return Response.status(Status.CONFLICT).build(); 
            }

            if (e.getClass() == ItemNotAcceptedException.class) {
                return Response.status(Status.NOT_ACCEPTABLE).entity(e.getMessage()).build(); 
            }

            e.printStackTrace();
            return Response.status(500).build();
        }
    }

    @PUT
    @Path("/{id}")
    public Response updateDetails(@PathParam("id") String id, T t) {
        try {
            this.sendCommand(()-> {
                return this.updateDetailsCommand(id, t);
            });

            return Response.ok().entity(queryService.getById(id)).build();
            
        } catch (ItemDoesNotExistException e) {

                return Response.status(Status.NOT_FOUND).build();

        } catch (Exception e) {

            if (e.getClass() == ItemDoesNotExistException.class) {
                return Response.status(Status.NOT_FOUND).build();
            }
            e.printStackTrace();
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }



    @GET
    public Response listAll(){
        try {         
            return Response.ok(this.getQueryService().listAll()).build(); 
                       
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("/{id}")
    public Response getById(@PathParam("id") String id) throws RuntimeException {
        try {
            return Response.ok(this.getQueryService().getById(id)).build();

        } catch(ItemDoesNotExistException e) {
            return Response.status(Status.NOT_FOUND).build();

        } catch(IllegalArgumentException e) {
            return Response.status(Status.BAD_REQUEST).build();

        } catch (RuntimeException e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    public Response deleteAll() {
        log.info("INSIDE DELETE ALL");
        try {
            JsonObject countJSON = this.sendCommand(()-> { 
                return this.getCommandService().deleteAll();
            })
            .get();     // Get the JsonObject from the Optional that is returned from sendCommand

            return Response.ok(countJSON.get("count").getAsLong()).build();

        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @SneakyThrows(ItemDoesNotExistException.class)
    public UUID deleteCommand(@NonNull UUID uuid) {
        return this.getCommandService().delete(uuid);
    }

    @DELETE
    @Path("/{id}")
    public Response deleteSpace(@PathParam("id") String id) {
        try {

            this.sendCommand(()-> {
                return this.deleteCommand(UUID.fromString(id));
            });

            return Response.ok().build();
            
        } catch (Exception e) {
            if (e.getClass() == ItemDoesNotExistException.class) {
                return Response.status(Status.NOT_FOUND).build();
            }
            e.printStackTrace();
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}