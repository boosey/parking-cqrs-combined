package org.boosey.parking.servicebase;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

public class ParkingEntityBase extends PanacheEntityBase implements ParkingEntityInterface {

    public String idAsString() {
        throw new RuntimeException("idAString() must be implemented by subclass");
    }
}