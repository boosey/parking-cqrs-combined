package org.boosey.parking.exceptions;

public class ItemNotAcceptedException extends Exception {
    static final long serialVersionUID = 1L;
    public ItemNotAcceptedException() {
        super("Item not accepted");
    }    
    
    public ItemNotAcceptedException(String message) {
        super(message);
    }
}