package org.boosey.parking.exceptions;

public class ItemDoesNotExistException extends Exception {
    static final long serialVersionUID = 1L;
    public ItemDoesNotExistException() {
        super("Item doesn't exist");
    }
}
