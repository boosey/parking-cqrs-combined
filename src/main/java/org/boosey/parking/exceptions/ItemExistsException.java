package org.boosey.parking.exceptions;

public class ItemExistsException extends Exception {
    static final long serialVersionUID = 1L;
    public ItemExistsException() {
        super("Item exists");
    }
}
