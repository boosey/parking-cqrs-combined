package org.boosey.parking.configuration;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import io.quarkus.arc.config.ConfigProperties;

@ConfigProperties(prefix = "parking")
public interface ParkingConfiguration {

    @ConfigProperty(name = "default-command-completion-timeout-milliseconds", defaultValue = "5000") 
    Long getTimeout();
}